/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <image_transport/camera_subscriber.h>
#include <image_transport/image_transport.h>
#include <image_transport/subscriber_filter.h>
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <nodelet/nodelet.h>
#include <pose_tracker_msgs/InitTracker.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <tf2_ros/message_filter.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <memory>
#include <pose_tracker/object_tracker/object_tracker.hpp>
#include <scigl_render/gl_context.hpp>

namespace pose_tracker
{
/**
 * Simplifies storing the configuration and execution based on it.
 */
class TrackerNodelet : public nodelet::Nodelet
{
public:
  /**
   * Initialization with ros_param configuration.
   */
  virtual void onInit();

  /*!
  Runs the tracker, will block until shutdown;
  */
  void run();

private:
  ros::NodeHandle nh;

  // PF tracker
  std::unique_ptr<pose_tracker::ObjectTracker> object_tracker;
  // transition model
  TransitionModel transition_model;
  // observation model
  DepthModelParams depth_params;
  std::unique_ptr<scigl_render::GLContext> gl_context;
  std::unique_ptr<LikelihoodEvaluator> likelihood_evaluator;
  std::string mesh_path;
  int particle_count;
  double delta_t;
  bool use_gpu_likelihoods;
  // if not using camera, use initial pose all the time
  bool use_camera_pose;
  ObjectState camera_pose;

  // transforms
  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener;
  tf2_ros::TransformBroadcaster tf_broadcaster;
  std::string camera_frame_id;
  std::string object_frame_id;
  std::string world_frame_id;
  // images
  std::unique_ptr<image_transport::ImageTransport> image_transport;
  image_transport::CameraSubscriber camera_sub;

  // publish insights of the particle filter
  ros::Publisher effective_samples_pub;
  ros::Publisher max_log_likelihood_pub;

  // services
  ros::ServiceServer init_service_server;

  /**
   * New camera image + info arrived, update the tracker, publish new map state.
   */
  void camera_callback(const sensor_msgs::ImageConstPtr& depth_image, const sensor_msgs::CameraInfoConstPtr& info);

  /**
   * Build the tracker for the given camera parameters.
   */
  void build_tracker(const sensor_msgs::CameraInfoConstPtr& info);

  /**
   * Service callback which sets an initial pose estimate.
   */
  bool init_service_callback(pose_tracker_msgs::InitTracker::Request& request,
                             pose_tracker_msgs::InitTracker::Response& response);

  /**
   * Load the private parameters into member variables.
   */
  void load_parameters();

  /**
   * Uses message filters for synchronized tf & camera subscription
   */
  void subscribe_synchronized();
};
}  // namespace pose_tracker