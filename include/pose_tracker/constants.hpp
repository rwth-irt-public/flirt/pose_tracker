/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <cmath>
#include <limits>

namespace pose_tracker {
const double NEG_INFINITY = -std::numeric_limits<double>::infinity();
const double LOG_2_PI = std::log(2 * M_PI);
const double LOG_2 = std::log(2);
const double SQRT_2 = std::sqrt(2);
const double HALF_SIGMA_PERCENT = std::erf(1 / 2 * SQRT_2);
const double SIGMA_PERCENT = std::erf(1 / SQRT_2);
const double TWO_SIGMA_PERCENT = std::erf(2 / SQRT_2);
const double THREE_SIGMA_PERCENT = std::erf(3 / SQRT_2);

}  // namespace pose_tracker
