/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <pose_tracker/model/object_state.hpp>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/buffer/frame_buffer.hpp>
#include <scigl_render/buffer/texture2d.hpp>
#include <scigl_render/render/depth_simulator.hpp>
#include <scigl_render/render/rasterizer.hpp>

namespace pose_tracker
{
/** renders the expected depth images, rasterized into a texture */
class DepthRasterizer
{
public:
  /**
   * for a given camera parametrization and a 3D model
   * \param intrinsics the cameras parameters
   * \param mesh_path the 3d model
   * \param particle_count creates a texture that is large enough for this many
   * particles
   */
  DepthRasterizer(const scigl_render::CameraIntrinsics& intrinsics, const std::string& mesh_path,
                  size_t particle_count);

  /**
   * render all the model poses in the current camera view
   */
  void render(const ObjectState& camera_state, const std::vector<ObjectState>& model_poses);

  std::shared_ptr<scigl_render::Texture2D> get_texture() const;
  std::shared_ptr<scigl_render::FrameBuffer> get_framebuffer() const;
  scigl_render::Rasterizer get_rasterizer() const;

private:
  scigl_render::DepthSimulator depth_simulator;
  scigl_render::Rasterizer rasterizer;
  std::shared_ptr<scigl_render::Texture2D> texture;
  std::shared_ptr<scigl_render::FrameBuffer> framebuffer;
};
}  // namespace pose_tracker
