/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <pose_tracker/object_tracker/likelihood_evaluator.hpp>
#include <scigl_render/shader/shader.hpp>

namespace pose_tracker
{
/**
 * Parametrizes and launches the compute shader to calculate the likelihoods
 * on the gpu
 */
class GpuLikelihoods : public LikelihoodEvaluator
{
public:
  GpuLikelihoods();
  /** construct and set the parameters of the depth observation model */
  GpuLikelihoods(const DepthModelParams& params);

  /**
   * Calculates the log_likelihoods of the observation given the expected
   * observations from the depth rasterizer
   *
   * \param observation the measurement from the depth camera
   * \param depth_rasterizer expectations from the rasterizer
   */
  std::vector<double> log_likelihoods(const DepthImage& observation, const DepthRasterizer& depth_rasterizer) override;

  /** set the parameters of the depth observation model */
  void set_params(DepthModelParams params) override;

private:
  scigl_render::Shader compute_shader;
  // dimension of the expectations
  size_t views_per_row = 0;
  size_t views_per_column = 0;
  // storage for the measured depth image
  std::unique_ptr<scigl_render::Texture2D> observation_tex;
  // shader storage buffer for the likelihoods
  GLuint likelihoods_ssbo;

  /** set the rendered expectations */
  void set_excpectations(std::shared_ptr<scigl_render::Texture2D> expectations, size_t views_per_row,
                         size_t views_per_column);

  /** set the observed depth image */
  void set_observation(const DepthImage& observation) const;
};
}  // namespace pose_tracker
