/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

R""(#version 430 core

// work balancing in local groups
layout (local_size_x = 128,
  local_size_y = 1,
  local_size_z = 1) in;
shared float local_likelihoods[gl_WorkGroupSize.x];

// constants
const float PI = 3.1415926535897932384626433832795;
// observation model, compare the CPU version
// x: width, y : height
uniform uvec2 image_size;
uniform float max_depth;
uniform float min_depth;
// gaussian distribution
uniform float gaussian_weight;
uniform float base_noise;
uniform float depth_factor;
// exponential distribution
uniform float exponential_weight;
uniform float lambda;
// uniform distribution
uniform float uniform_weight;
uniform float uniform_density;

// from the depth camera
layout(binding = 0, r32f) readonly uniform image2D observed_image;
// texture with all the rendered depth images
layout(binding = 1, r32f) readonly uniform image2D expected_image;
// log likelihoods
layout(binding = 2) writeonly buffer Likelihoods
{
  float likelihoods[];
};

// returns x: column, y: row
uvec2 pos_in_image(uint index)
{
  uvec2 pos;
  pos.x = uint(mod(index, image_size.x));
  pos.y = index / image_size.x;
  return pos;
}

// index of the current workgroup
uint workgroup_index()
{
  return gl_WorkGroupID.z * gl_NumWorkGroups.x * gl_NumWorkGroups.y +
    gl_WorkGroupID.y * gl_NumWorkGroups.x + 
    gl_WorkGroupID.x;
}

// pow(value, 2)
float square(float value)
{
  return value * value;
}

// is the measurement possible?
bool is_valid(float depth)
{
  return depth > 0 && depth < max_depth;
}

// gaussian probability of the beam hitting the object
float gaussian_probability(float expected, float observed)
{
  float sigma = depth_factor * square(expected) + base_noise;
  float two_variance = 2 * square(sigma);
  // normalization factor is approx 1 for small variances
  return exp(-square(observed - expected) / two_variance) /
         sqrt(PI * two_variance);
}

// exponential probability of beeing occluded
float exponential_probability(float expected, float observed)
{
  float numerator = exp(-lambda * observed);
  float denominator = 1 - exp(-lambda * expected);
  return lambda * numerator / denominator;
}

// logarithmic
float log_likelihood(float expected, float observed)
{
  if(!is_valid(expected))
  {
    // expected invalid: Any measurement is okay log(1)=0, check this condition first!
    return 0;
  }
  if(!is_valid(observed))
  {
    // observed invalid: exponential would divide by 0, normal is so small that log would return -inf so ignore them
    return log(uniform_weight * uniform_density);
  }
  if (observed <= expected)
  {
    // occlusion is possible
    return log(uniform_weight * uniform_density +
      gaussian_weight * gaussian_probability(expected, observed) +
      exponential_weight * exponential_probability(expected, observed));
  }
  else
  {
    // occlusion impossible
    return log(uniform_weight * uniform_density +
      gaussian_weight * gaussian_probability(expected, observed));
  }
}

// Calculates the log_likelihood of the depth images.
// One workgroup calculates one images likelihood.
// The pixels are evenly distributed over the local threads.
void main()
{
  float thread_likelihood = 0;
  uint local_size = gl_WorkGroupSize.x;
  uint pixels_per_thread = uint(ceil(
    float(image_size.x * image_size.y) / local_size));
  // position of top left image pixel
  uvec2 group_pos = gl_WorkGroupID.xy * image_size;
  for(uint i = 0; i < pixels_per_thread; i++)
  {
    // position inside the image
    uint pixel_id = i * local_size + gl_LocalInvocationIndex;
    uvec2 image_pos = pos_in_image(pixel_id);
    if(image_pos.x < image_size.x && image_pos.y < image_size.y)
    {
      // position in rendered texture
      uvec2 pixel_pos =  group_pos + image_pos;
      float expected = imageLoad(expected_image, ivec2(pixel_pos)).r;
      float observed = imageLoad(observed_image, ivec2(image_pos)).r;
      // add log_likelihoods
      thread_likelihood += log_likelihood(expected, observed);
    }
  }
  // synchronized accumulation local log likelihoods
  local_likelihoods[gl_LocalInvocationIndex] = thread_likelihood;
  memoryBarrierShared();
  barrier();
  if(gl_LocalInvocationIndex == 0)
  {
    float group_likelihood = 0;
    for(uint i = 0; i < local_size; i++)
    {
      group_likelihood += local_likelihoods[i];
    }
    likelihoods[workgroup_index()] = group_likelihood;
  }
})""