/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <memory>
#include <pose_tracker/gpu/depth_rasterizer.hpp>
#include <pose_tracker/model/depth_image.hpp>
#include <pose_tracker/model/depth_model_params.hpp>
#include <scigl_render/buffer/texture2d.hpp>

namespace pose_tracker
{
/**
 * Interface to compare the observation and depth renderings to calculate the
 * likelihoods
 */
class LikelihoodEvaluator
{
public:
  /**
   * Calculates the log_likelihoods of the observation given the expected
   * observations from the depth rasterizer
   *
   * \param observation the measurement from the depth camera
   * \param depth_rasterizer expectations from the rasterizer
   */
  virtual std::vector<double> log_likelihoods(const DepthImage& observation,
                                              const DepthRasterizer& depth_rasterizer) = 0;

  /** set the parameters of the depth observation model */
  virtual void set_params(DepthModelParams params) = 0;
};
}  // namespace pose_tracker