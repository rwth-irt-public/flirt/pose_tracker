/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <flirt/particle_filter/log_particle_filter.hpp>
#include <pose_tracker/gpu/depth_rasterizer.hpp>
#include <pose_tracker/model/depth_image.hpp>
#include <pose_tracker/model/transition_model.hpp>
#include <pose_tracker/object_tracker/likelihood_evaluator.hpp>
#include <scigl_render/scene/camera_intrinsics.hpp>

namespace pose_tracker
{
/**
Tracks an object, evaluating the likelihoods on CPU
*/
class ObjectTracker
{
public:
  /**
   * Create a fully parametrized object tracker
   * @param particle_count the number of simualted particles
   * @param transition_model transition like a random walk
   * @param likelihood_impl the implementation of the likelihood evaluator
   * @param mesh_path 3d model to track
   * @param intrinsics camera parameters
   */
  ObjectTracker(size_t particle_count, TransitionModel transition_model,
                std::unique_ptr<LikelihoodEvaluator> likelihood_impl, const std::string& mesh_path,
                const scigl_render::CameraIntrinsics& intrinsics);

  /**
   * Returns the effective sample size of the underlying particle filter.
   */
  double effective_sample_size() const;

  /**
   * Returns the maximum log-likelihood from the last update step.
   */
  double max_log_likelihood() const;

  /**
   * Initializes the filter with the given pose. Applies some noise even before the first (noisy) prediction to gain
   * more robustness against imprecise initializations.
   * @param object_state the inital pose of the object
   * @param delta_t time step in seconds
   * @param replacement_ration ratio of particles to replace [0...1]
   */
  void initialize(ObjectState object_state, double delta_t, double replacement_ratio = 0.3);

  /**
   * Executes one filter step: predicts the next particle states and updates their likelihoods given the observation.
   * @param observation the depth image as data from
   * @param camera_state the pose of the camera
   * @param delta_t time step in seconds
   * @returns the updated maximum-a-posteriori state
   */
  ObjectState next_frame(const DepthImage& observation, const ObjectState& camera_state, double delta_t);

private:
  flirt::LogParticleFilter<ObjectState> filter;
  // transitions
  TransitionModel transition_model;
  // observations
  DepthRasterizer depth_rasterizer;
  std::unique_ptr<LikelihoodEvaluator> likelihood_evaluator;

  /** calculates the mean of the states */
  ObjectState mean_state(const std::vector<double>& weights, const std::vector<ObjectState>& states) const;

  /**
   * predicts the next states and expected measurements (starts reading)
   * @param camera_state the current location of the camera
   * @param delta_t time step in seconds
   */
  void predict(const ObjectState& camera_state, double delta_t);

  /**
   * updates the likelihoods with the observation (ends reading)
   * @param observation the depth image observed by the camera
   */
  void update(const DepthImage& observation);
};
}  // namespace pose_tracker