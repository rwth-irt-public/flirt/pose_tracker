/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <pose_tracker/model/object_state.hpp>
#include <scigl_render/scene/pose.hpp>

namespace pose_tracker
{
class RoboToScigl
{
public:
  /**
   * Convert the pose of the state to a renderable quaternion pose
   */
  static scigl_render::QuaternionPose convert_pose(const ObjectState& state);
};
}  // namespace pose_tracker