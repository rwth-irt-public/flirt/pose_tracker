/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <opencv2/core/core.hpp>

namespace pose_tracker
{
/*!
Specification of the internally used depth image representation. The image must
have its origin (0,0) at the bottom left to avoid transforming the much greater
number of OpenGL rendered images.
Except this the class is intendet to be compatible to the ROS canonicial depth
image representation as described in
(REP 118)[http://www.ros.org/reps/rep-0118.html]. The depth is measured in
meters and the corresponding OpenCV encoding is a single channel float encoding:
"32FC1".
Make sure to create a continous OpenCV Mat object via the Mat::create method!
*/
using DepthImage = cv::Mat_<float>;
}  // namespace pose_tracker