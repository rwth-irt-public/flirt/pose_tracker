/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <cstdint>
#include <pose_tracker/model/depth_model_params.hpp>

namespace pose_tracker {
/**
 * Modelling the pixel depth probabilities via a beam based model similar to
 * the one from "Probabilistic Robotics"[Thru.06](P.174). It is basically the
 * sum of different distributions (hit, short and random). Invalid values (0 or
 * infinity) are ignored. This is an observation model.
 * All depth values are assumed to be according the REP-118: floats in meters.
 */
class DepthPixelModel {
 public:
  /**
   * Create a parametrized observation model.
   * \param measurement_params parametrization of the depth pixel model
   */
  DepthPixelModel(DepthModelParams measurement_params);

  /**
   * It is basically the sum of different distributions: gaussian for measuring
   * the objects depth, exponential for occlusions, discrete for failure and
   * uniform for random measurements. Invalid values (0 or infinity) are handled
   * in this function.
   * \param expected the simulated depth value in meters
   * \param observed the observed depth value in meters
   * \return logarithm of the likelihood
   */
  double log_likelihood(float expected, float observed) const;

  /**
   * The ideal log likelihood is calculated for the expectation. Ideal is seen
   * if the measurement lays within three sigma interval (99,73% of noise).
   * Likelihood of a valid & non-occluded pixel.
   * \param expected the expected depth value
   */
  double ideal_log_likelihood(const float &expected) const;

  /*!
  Squares the number a.
  */
  static inline double square(double a) { return a * a; }

 private:
  DepthModelParams params;

  /*!
  Returns true if the depth measurement/rendering is valid.
  It must be element of (0, max_depth)
  */
  bool is_valid(float depth) const;

  /**
   * The standard deviation of the camera noise grows exponentially with the 
   * depth
   */
  double sigma_for_depth(float expected) const;

  /*!
  Calculates the (normalized) gaussian probability for the actual value.
  The normal distribution is parametrized by the expected value and the standard
  deviation.
  */
  double gaussian_probability(float expected, float observed) const;

  /*!
  Calculates the normalized exponential probability for the actual value.
  The exponential distribution is parametrized by the decay constant lambda.
  */
  double exponential_probability(float lambda, float expected, float observed) const;
};
}  // namespace pose_tracker