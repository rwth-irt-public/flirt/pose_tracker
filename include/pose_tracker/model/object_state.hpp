/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <tf2/LinearMath/Transform.h>

namespace pose_tracker
{
/*! The state of an object for the 6D pose tracker */
struct ObjectState
{
  /*! The current position of the object */
  tf2::Vector3 position;

  /*! The current orientation of the object */
  tf2::Quaternion orientation;

  /*! Translational velocity of the object */
  tf2::Vector3 velocity;

  /*! Angular velocity in terms of an quaternion with (w_x, w_y, w_z) */
  tf2::Vector3 angular_velocity;

  /*!
  Initializes to zero
  */
  ObjectState() : position(0, 0, 0), orientation(0, 0, 0, 1), velocity(0, 0, 0), angular_velocity(0, 0, 0)
  {
  }
};
}  // namespace pose_tracker