/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <cstddef>
#include <cstdint>
#include <pose_tracker/model/depth_image.hpp>
#include <pose_tracker/model/depth_pixel_model.hpp>
#include <vector>

namespace pose_tracker {
/**
 * Uses a rendered depth image in combination with the beam based
 * DepthPixelModel to compute the (log)likelihood of a measured image.
 * Notice that the depth type is according the OpenNI RAW representation:
 * http://www.ros.org/reps/rep-0118.html so all vaules are unsigned shorts for
 * the depth in millimeters.
 */
class DepthImageModel {
 public:
  /**
   * Create a parametrized observation model.
   * \param depth_params parameters of the depth camera
   */
  DepthImageModel(DepthModelParams depth_params);

  /**
   * Calculates the accumulated likelihood for a depth measurement given the
   * expected/simulated depths. The pixels measurements are assumed to be
   * independent. Using raw pointers to avoid expensive copying from OpenGL
   * mapped buffer to stl containers.
   * \param expectation the depth image from the camera
   * \param prediction the depth image from OpenGL, use it directly from the
   * mapped memory, therefore the raw pointer\
   * return logarithm of the likelihood
   */
  double log_likelihood(const DepthImage &expectation,
                        const DepthImage &observation) const;

  /**
   * Likelihood of an observation, that has exactly the right pose, no 
   * occlusions and no invalid pixels. This is just the gaussian model within
   * three sigma.
   */
  double ideal_log_likelihood(const DepthImage &expectation);

 private:
  DepthPixelModel pixel_model;
};
}  // namespace pose_tracker