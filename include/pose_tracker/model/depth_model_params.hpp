/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <cmath>

namespace pose_tracker
{
/**
 * Contains the parameters of a depth measurement. This is hardware specific
 * for each camera.
 */
struct DepthModelParams
{
  /** the minimal depth measurement of the camera [meters] */
  double min_depth;
  /** the maximal depth measurement of the camera [meters] */
  double max_depth;
  /** horizontal size of the depth image */
  size_t width;
  /** vertical size of the depth image */
  size_t height;
  /** some base noise [meters] b of the measurement. s(z)=c*z^2 + b  */
  double base_noise;
  /** constant c for variance. s(z)=c*z^2 + b */
  double depth_factor;
  /** occlusion probability is halfed after this distance [meters] */
  double half_life_distance;
  /** for exponential distribution from the half life distance */
  double get_lambda() const
  {
    return std::log(2) / half_life_distance;
  }
  /** weight of the gaussian distribution (actual target) */
  double gaussian_weight;
  /** weight of the exponential distribution (occlusion) */
  double exponential_weight;
  /*! weight of the likelihood of random measurements */
  double uniform_weight;
  /** returns the uniform probability density for max depth */
  double get_uniform_density() const
  {
    return 1 / max_depth;
  }

  void normalize_weights()
  {
    double factor = 1 / (gaussian_weight + exponential_weight + uniform_weight);
    gaussian_weight *= factor;
    exponential_weight *= factor;
    uniform_weight *= factor;
  }
};
}  // namespace pose_tracker