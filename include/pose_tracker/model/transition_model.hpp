/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <memory>
#include <pose_tracker/model/object_state.hpp>

// forward declare the normal_sampler
namespace flirt
{
class NormalSampler;
}

namespace pose_tracker
{
/**
 * Random walk process imposes gaussian noise to the current velocity which is integrated into the current position
 */
class TransitionModel
{
public:
  /**
   * Default constructor. Don't forget to set the properties!
   */
  TransitionModel();

  /**
   * For testing use a non random seed
   */
  TransitionModel(unsigned int seed);

  /**
   * Predicts the next state given the current state and an acceleration. Compare the quaternion state space model:
   * http://campar.in.tum.de/Chair/KalmanFilter and https://arxiv.org/abs/1711.02508
   * @param state the current state of the system
   * @param acceleration change of the velocity
   * @param angular_acceleration change of the angular velocity
   * @param delta_t how far to look into the future
   */
  ObjectState predict(ObjectState state, tf2::Vector3 acceleration, tf2::Vector3 angular_acceleration,
                      double delta_t) const;

  /**
   * Executes a prediction with random accelerations.
   * @param state the current state of the system
   * @param delta_t how far to look into the future
   */
  ObjectState predict_noisy(ObjectState state, double delta_t);

  /**
   * Returns an acceleration vector sampled from a normal distribution with 0 mean and translational_noise as
   * standard deviation.
   */
  tf2::Vector3 random_acceleration();

  /**
   * Returns an angular acceleration vector sampled from a normal distribution with 0 mean and rotational_noise as
   * standard deviation.
   */
  tf2::Vector3 random_angular_acceleration();

  /**
   * Noise in the prediction of the next position and velocity.
   * @param translational_noise standard deviation of the angular acceleration
   */
  void set_translational_noise(double translational_noise);
  /**
   * Noise in the prediction of the next orientation and angular velocity
   * @param rotational_noise standard deviation of the acceleration
   */
  void set_rotational_noise(double rotational_noise);

  /**
   * Set the factor to [0,1] and the velocity will decay at this rate
   */
  void set_velocity_factor(double factor = 0.8);

private:
  // Impose randomness to the state transition
  std::shared_ptr<flirt::NormalSampler> normal_sampler;
  double rotational_noise;
  double translational_noise;
  double velocity_factor;
};
}  // namespace pose_tracker
