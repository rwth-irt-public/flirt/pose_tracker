
# Table of Contents
[[_TOC_]]

# About
Object tracking library based on a particle filter, which utilizes a ray based depth model.
The rigid body model is rendered in every timestep and compared to the observed depth values.

# Requirements
An OpenGL driver supporting core profile 4.2 is required.
NVIDIA drivers seem to work well, we experienced some weird issues with the Intel Mesa drivers.
You can check your version via:
```bash
glxinfo | grep 'version'
```

# Install
Two different approaches can be taken to install and setup this package:
1. [Docker](https://www.docker.com/): The [Dockerfile](Dockerfile) is used to setup the whole environment with all dependencies in an encapsulated container.
2. Manual setup: If you have Ubuntu 18.04 installed you can setup the environment using as set of ROS tools.

## Docker installation
Please [install](https://docs.docker.com/engine/install/) Docker for your system, for example:
- [Windows installation guide](https://docs.docker.com/docker-for-windows/install/)
- [Ubuntu installation guide](https://docs.docker.com/engine/install/ubuntu/)
Also, make sure to execute the [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/) to be able to run docker as non-root.

Since the particle filter uses OpenGL, two more packages have to be installed:
- [NVIDIA Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/overview.html): Ubuntu installation is basically `sudo apt install nvidia-docker2`.
  On their homepage more detailed [installation instructions}(https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) can be found.
- [rocker](https://github.com/osrf/rocker): In Debian based systems install rocker via `sudo apt install python3-rocker`.
  For other systems, please check out their [github page](https://github.com/osrf/rocker).

After installing the packages, enable and start Docker:
```bash
systemctl enable docker # root: sudo, rootless: --user
systemctl start docker  # root: sudo, rootless: --user
```

Now, the container can be started in interactive mode with GUI/OpenGL support via rocker.
After starting the shell, source the catkin project and launch the demo.
```bash
rocker --nvidia --pull --x11 --name posetracker registry.gitlab.com/rwth-irt-public/flirt/pose_tracker:latest
# launch the demo after the container started
roslaunch pose_tracker demo.launch
```

Spinning up another interactive session in the same container can be done via:
```bash
docker exec -it posetracker bash
```

### Development Container
For the development inside a container, Visual Studio Code can be used with the [Remote-Containers Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).
Vscode usual workflow seems to be opening a local folder in a remote container.
Unfortunately, this workflow does not allow to debug the nodes which require OpenGL.
For these cases vscode can be attached to an existing container.

Use rocker with the `--nocleanup` option to prevent removing the docker container (and all your progress) after stopping the container.
Optionally, the `--git` option allows you to use the global Git settings from the host.
```bash
rocker --git --nocleanup --nvidia --pull --x11 --name posetracker registry.gitlab.com/rwth-irt-public/flirt/pose_tracker:latest
```
> At the time of writing [pull 111](https://github.com/osrf/rocker/pull/111) has to be applied for the `--nocleanup` option
> Clone the repository with the fix or checkout the pull request and run:
> ```bash
> # Install the source version
> python setup.py install
> # Test NVIDIA hardware access via rocker
> rocker --nvidia nvidia/cuda:11.0-base nvidia-smi
> ```

In vscode follow the [instructions](https://code.visualstudio.com/docs/remote/attach-container) for attaching to a running container. 
> Tipp: Setup the configuration for the development container.
> Attach to the container, hit F1 and select *Remote-Containers: Open Named Configuration File*.
> The [devcontainer.json](.devcontainer/devcontainer.json) file can serve as a starting point for your configuration.
> After setting up the configuration, the container has to be removed and recreated with the same name, to apply all changes.

### Notes on ROS in Docker
By default, a roscore instance inside a container cannot be accessed by the host system.
The other way around does not work, either.
This is however not the docker way of composing a system.
The preferred way would be to setup different containers for different components and compose them in a docker network.
An example docker-compose.yml file can be found [here](https://github.com/toddsampson/ros-docker/blob/master/docker-compose.yml).

### Hardware acceleration with rootless docker
If you do not have root rights or do not want to setup a docker group which grants root-like privileges, docker can be installed in [rootless mode](https://docs.docker.com/engine/security/rootless/).
Right now, this cannot be used with rocker and the NVIDIA docker toolkit out of the box.

After installing Docker rootless, install the [NVIDIA docker toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html), for example via `sudo apt install nvidia-docker2`.
To allow hardware access for non-roots, a root user must modify */etc/nvidia-container-runtime/config.toml*. More infos can be found in this [github issue](https://github.com/containers/podman/issues/3659#issuecomment-543912380).
```conf
[nvidia-container-cli]
no-cgroups = true
```
> At the time of writing rocker also had a bug and the [fix](https://github.com/osrf/rocker/issues/109) has to be installed from source.
> Clone the repository with the fix or checkout the pull request and run:
> ```bash
> # Install the source version
> python setup.py install
> # Test NVIDIA hardware access via rocker
> rocker --nvidia nvidia/cuda:11.0-base nvidia-smi
> ```
You should see an output similar to:
```bash
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.51.06    Driver Version: 450.51.06    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
```

## Manual Setup Prerequisites
The following components should be installed on your system
- [Ubuntu 18.04](https://releases.ubuntu.com/18.04/): If you do not have this version installed, we recommend using the [docker container](Dockerfile) since some dependencies require custom installation steps.
- [ROS Melodic Desktop-Full](http://wiki.ros.org/melodic): Please carefully follow the complete [installation guide]((https://wiki.ros.org/melodic/Installation/Ubuntu)).
- [wstool](http://wiki.ros.org/wstool): Simplified workspace management, setup and updates. Install via `sudo apt install python-wstool`.
- [catkin-tools](https://catkin-tools.readthedocs.io/en/latest/index.html): Parallelized compilation and support for mixed workspaces. Please follow their [installation guide](https://catkin-tools.readthedocs.io/en/latest/installing.html).
- [git-lfs](https://packagecloud.io/github/git-lfs/install): Used to download large files like mesh files.
  A more comprehensive guide can be found on [github](https://github.com/git-lfs/git-lfs/wiki/Installation).
  If not installed before, you can install curl via `sudo apt install curl`

## Setup catkin workspace

Setup the a catkin workspace via the `wstool`.
You might want to change the URL in the [.rosinstall](.rosinstall) file whether you use SSH or HTTPS to clone repositories.

```bash
# init workspace
mkdir -p catkin_ws && cd catkin_ws
wstool init src
catkin init
# add pose_tracker
wstool set -y -t src --git pose_tracker https://gitlab.com/rwth-irt-public/flirt/pose_tracker.git
wstool update -t src
# add and download dependencies of scigl_render
wstool merge -t src src/pose_tracker/.rosinstall
wstool update -t src
# install system dependencies via rosdep:
rosdep install --from-paths src --ignore-src -r -y
```

# Build the Catkin Workspace
As `catkin_make` does not support mixed CMake and catkin workspaces, you must use either `catkin_make_isolated` oder `catkin build` to compile the workspace.
We recommend the latter for faster compile times on multi-core system.
It is part of the [catkin_tools](https://catkin-tools.readthedocs.io/en/latest/index.html).

# Unit Tests
Under Ubuntu GTest only installs the sources.
To build GTest run:
```sh
sudo apt install libgtest-dev build-essential cmake
cd /usr/src/googletest
sudo cmake .
sudo cmake --build . --target install
```

You can use the following commands to run the tests
```bash
cd /path/to/catkin_ws
# release optimizations cause tests to fail
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Debug
# rebuild
catkin build pose_tracker
# run tests manually
catkin test --no-deps pose_tracker
```

# Usage
The pose tracker runs a particle filter (from the [Flirt Core Library](https://gitlab.com/rwth-irt-public/flirt/flirt.git)) which uses depth images to infer the posterior pose of the object.
For the calculation of the likelihoods in the update step, a 3D-model is rendered via [scigl_render_ros](https://gitlab.com/rwth-irt-public/flirt/scigl_render_ros.git) and compared to the measured depth images.
Since the 6D pose space is too large for a global localization, a coarse initial pose estimate has to be given to the filter.

As the image processing is quite resource intensive, we suggest using the following optimizations:
- GPU implementation of the observation model (paramteter `use_gpu_likelihoods`)
- Preprocess the depth images `image_proc` and downscale them via `image_proc/resize`.
  We downscale the images to *80x60px* - higher resolutions are only required for tiny objects.
- For robustness, prefer higher tracking rates over higher resolutions or particle counts.
  This drastically reduces the uncertainty between the update steps.
- Use the `tracker_nodelet` in the same nodelet manager as your image pipeline.
  We did however experience some unsolved crashes, which did not occur when using the tracker nodelet as `standalone`.

We provide an example launch file which does not require a depth camera but renders a dummy motion via [scigl_render_ros](https://gitlab.com/rwth-irt-public/flirt/scigl_render_ros.git) instead: [launch/demo.launch](launch/demo.launch).
After building the workspace run:
```bash
cd /path/to/catkin_ws
source devel/setup.bash
roslaunch pose_tracker demo.launch 
```

![Output of depth_render_node](images/pose_tracker_demo.png)

*RViz screenshot of the demo.launch.*

# tracker_nodelet
## Topics
Subscribed:
- `/camera/depth/resized/image_rect` : camera image to render on top, also subscribes the matching camera info via `image_transport`
- `/tf` : transformation of the camera (if `use_camera_pose`)

Published:
- `/effective_samples_size` : effective samples size of the current particle set
- `/maximum_log_likelihood` : maximum logarithmic likelihood of the current particle set
- `/tf`: pose  of the object as transformation

## Services
-  `init_tracker` : Set an initial pose estimate with a defined particle replacement ratio, as defined in [pose_tracker_msgs](https://gitlab.com/rwth-irt-public/flirt/pose_tracker_msgs/-/blob/master/srv/InitTracker.srv).
  For example, the [init_from_tf.py](https://gitlab.com/rwth-irt-public/flirt/pose_tracker_msgs/-/blob/master/scripts/init_from_tf.py) script can be used.

## Parameters
The probabilistic model has quite a few parameters, so we recommend using `rosparam` to load the parameters from an YAML file, for example [parameters/tracker.yaml](parameters/tracker.yaml).
Generally, prefer overestimating the noise of your model (as stated in "Probabilistic Robotics", by Thrun et al.).

General:
- `particle_count` : Number of particles which approximate the pose posterior.
  Higher numbers increase the precision and robustness.
  Increase the number as long as the tracker can keep up with the camera's frame rate.
- `frame_rate` : The frame rate of the camera is assumed to be fixed - set this number to match your camera configuration.
  Experiments with a variable update rate have lead to failures with missing messages.
- `mesh_path` : path of the 3D model to track
- `world_frame_id` : published pose estimates will use this as parent frame.
- `object_frame_id` : published pose estimates will use this as child frame.
- `use_gpu_likelihoods` : Always recommended
- `use_camera_pose` : If the pose of the camera in the world frame is known, for example via a robots forward kinematics.

Transition model (decaying velocity model), these actually need some careful tuning:
- `translational_noise` : standard deviation of the predicted velocity in *m/s*
- `rotational_noise` : standard deviation of the predicted velocity in *rad/s*
- `velocity_factor` : exponential decay of the velocity in each prediction step, 0.9 is good for robust motion tracking 0.0 is good for static scenes.

Camera model (beam based range camera):
- `depth_factor` : factor of the quadratically increasing standard deviation of stereo camera measurements, in *1/m*
- `base_noise` : this noise is added to the depth dependant noise, in *m*
- `min_depth` : near plane of the virtual camera frustum, set it to the minimal measurable distance of your camera model, in *m*
- `max_depth` : far plane of the virtual camera frustum, set it to the minimal measurable distance of your camera model, in *m*

Observation model distributions (weighted sum of an exponential, normal and uniform distribution), the values barely matter as long as they are all non-zero:
- `exponential_weight` : influence of occlusions
- `gaussian_weight` : influence of expected measurement
- `uniform_weight` : influence of unexpected / missing measurements

# Citing
If you use parts of this library in your scientific publication, please consider citing:
```
@article { 3Dcamerabasedmarkerlessnavigationsystemforroboticosteotomies,
      author = "Tim Übelhör and Jonas Gesenhues and Nassim Ayoub and Ali Modabber and Dirk Abel",
      title = "3D camera-based markerless navigation system for robotic osteotomies",
      journal = "at - Automatisierungstechnik",
      year = "01 Oct. 2020",
      publisher = "De Gruyter",
      address = "Berlin, Boston",
      volume = "68",
      number = "10",
      doi = "https://doi.org/10.1515/auto-2020-0032",
      pages=      "863 - 879",
      url = "https://www.degruyter.com/view/journals/auto/68/10/article-p863.xml"
}
```

# Funding
Funded by the Excellence Initiative of the German federal and state governments.

# Related Work
The filtering is inspired by this work:
https://github.com/bayesian-object-tracking/dbot
which is based on this paper:
```
inproceedings{wuthrich-iros-2013,
title = {Probabilistic Object Tracking Using a Range Camera},
author = {W{\"u}thrich, M. and Pastor, P. and Kalakrishnan, M. and Bohg, J. and Schaal, S.},
booktitle = {IEEE/RSJ International Conference on Intelligent Robots and Systems},
pages = {3195-3202},
publisher = {IEEE},
month = nov,
year = {2013},
month_numeric = {11}
}
```

However there are some differences:

- We use a simpler observation model, which is similar to the one from the book `Probabilistic Robotics by Sebastian Thrun (2015)` (http://www.probabilistic-robotics.org/) and does NOT include explicit occlusion handling.
  However, we found that implicit occlusion handling is sufficient and enables a high tracking rate, which is critical to avoid uncertainties.
- Improved logarithmic formulation of the particle filter and observation equations, based on Gentner et al.
- Supporting diverse 3D model data formats
- Supplying a [render engine](https://gitlab.com/rwth-irt-public/flirt/scigl_render_ros) for visualization and simulating depth values

Original log-particle filter paper:
```
Christian Gentner, Siwei Zhang, and Thomas Jost,
“Log-PF: Particle Filtering in Logarithm Domain,” 
Journal of Electrical and Computer Engineering, vol. 2018, 
Article ID 5763461, 11 pages, 2018. https://doi.org/10.1155/2018/5763461.
```
