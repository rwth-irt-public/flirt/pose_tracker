# @license BSD-3 https://opensource.org/licenses/BSD-3-Clause
# Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
# All rights reserved. 

FROM osrf/ros:melodic-desktop
# basic packages
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash && \
  apt-get update && \
  apt-get install git-lfs -y && \
  git lfs install
RUN apt-get update && apt-get install wget
# ROS packages
RUN apt-get update && apt-get install -y \
  python-catkin-tools python-wstool
# Unit Tests
RUN apt-get update && apt-get install \
  libgtest-dev build-essential cmake && \
  cd /usr/src/googletest && \
  cmake . && cmake --build . --target install
# Workspace setup, bash from now on
SHELL ["/bin/bash", "-c"]
WORKDIR "/catkin_ws"
RUN echo "source /catkin_ws/devel/setup.bash" >> /root/.bashrc
RUN source /ros_entrypoint.sh && \
  wstool init src  && \
  catkin init
COPY . src/pose_tracker
RUN ls && wstool merge -t src src/pose_tracker/.rosinstall && \
  wstool update -t src
RUN rosdep install --from-paths src --ignore-src -r -y
# Build
RUN source /ros_entrypoint.sh && catkin build