/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <gtest/gtest.h>
#include <cmath>
#include <pose_tracker/model/depth_pixel_model.hpp>

const double EPS = 1e-6;
// maximum depth in meters
const float MAX_DEPTH = 10;
const double DEPTH__FACTOR = 3.75e-3;
const double MODEL_STANDARD_DEVIATION = 9e-3;
const double HALF_LIFE_DISTANCE = 1;
// weights times 10 to test the normalization
const double WEIGHT_FACTOR = 0.1;
const double GAUSSIAN_WEIGHT = 7;
const double EXPONENTIAL_WEIGHT = 2;
const double UNIFORM_WEIGHT = 1;

pose_tracker::DepthPixelModel create_model()
{
  pose_tracker::DepthModelParams params;
  params.max_depth = MAX_DEPTH;
  params.depth_factor = DEPTH__FACTOR;
  params.base_noise = MODEL_STANDARD_DEVIATION;
  params.half_life_distance = HALF_LIFE_DISTANCE;
  params.gaussian_weight = GAUSSIAN_WEIGHT;
  params.exponential_weight = EXPONENTIAL_WEIGHT;
  params.uniform_weight = UNIFORM_WEIGHT;
  return pose_tracker::DepthPixelModel(params);
}

/*!
Calculates the likelihood in regular domain
*/
double likelihood(double expected, double measured)
{
  double gauss_prob = 0;
  double exp_prob = 0;
  double uniform_prob = 1 / MAX_DEPTH;
  if (0 < measured && measured < MAX_DEPTH && 0 < expected && expected < MAX_DEPTH)
  {
    double depth_deviation = DEPTH__FACTOR * std::pow(measured, 2) + MODEL_STANDARD_DEVIATION;
    double variance = std::pow(depth_deviation, 2);
    gauss_prob = 1 / std::sqrt(2 * M_PI * variance) * std::exp(-std::pow(expected - measured, 2) / (2 * variance));
    // Test this!
    if (measured <= expected)
    {
      double exponential_decay = std::log(2) / HALF_LIFE_DISTANCE;
      exp_prob = exponential_decay * std::exp(-exponential_decay * measured) / (1 - exp(-exponential_decay * expected));
    }
  }
  // only valid cases are calculated so invalid_prob is 0
  return WEIGHT_FACTOR * (GAUSSIAN_WEIGHT * gauss_prob + EXPONENTIAL_WEIGHT * exp_prob + UNIFORM_WEIGHT * uniform_prob);
}

TEST(DepthPixelTest, TestFailureLikelihood)
{
  auto model = create_model();
  // Test out of sensor field, random prob
  double log_failure_prob = std::log(WEIGHT_FACTOR * UNIFORM_WEIGHT / MAX_DEPTH);
  float valid = MAX_DEPTH / 2;
  // invalid expected value --> log(1)=0 likelihood
  // too short
  float tooshort = -1;
  ASSERT_DOUBLE_EQ(log_failure_prob, model.log_likelihood(valid, tooshort));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(tooshort, valid));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(tooshort, tooshort));
  // corner cases short
  ASSERT_DOUBLE_EQ(log_failure_prob, model.log_likelihood(valid, 0));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(0, valid));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(0, 0));
  // too far
  float far = MAX_DEPTH + 1;
  ASSERT_DOUBLE_EQ(log_failure_prob, model.log_likelihood(valid, far));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(far, valid));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(far, far));
  // corner cases far
  ASSERT_DOUBLE_EQ(log_failure_prob, model.log_likelihood(valid, MAX_DEPTH));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(MAX_DEPTH, valid));
  ASSERT_DOUBLE_EQ(0, model.log_likelihood(MAX_DEPTH, MAX_DEPTH));
}

TEST(DepthPixelTest, TestLikelihood)
{
  auto model = create_model();
  // exact hit
  float expected = MAX_DEPTH / 2;
  float measured = expected;
  ASSERT_NEAR(std::log(likelihood(expected, measured)), model.log_likelihood(expected, measured), EPS);
  // occlusion
  measured = expected / 2;
  ASSERT_NEAR(std::log(likelihood(expected, measured)), model.log_likelihood(expected, measured), EPS);
  // measurment behind object
  measured = expected * 3 / 4;
  ASSERT_NEAR(std::log(likelihood(expected, measured)), model.log_likelihood(expected, measured), EPS);
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
