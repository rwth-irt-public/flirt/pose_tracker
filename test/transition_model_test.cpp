/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <gtest/gtest.h>
#include <pose_tracker/model/transition_model.hpp>

using namespace pose_tracker;

const double EPS = 0.1;
const double POSITION_STANDARD_DEV = 1;
const double POSITION_VAR = POSITION_STANDARD_DEV * POSITION_STANDARD_DEV;
const double ORIENTATION_STANDARD_DEV = 0.02;
const double VELOCITY_FACTOR = 0.5;
// const double ORIENTATION_VAR = ORIENTATION_STANDARD_DEV * ORIENTATION_STANDARD_DEV;
const double VAR_EPS = 0.15;
const size_t N_SAMPlES = 10000;

void vec_approx(const tf2::Vector3& a, const tf2::Vector3& b, double eps)
{
  ASSERT_NEAR(a.getX(), b.getX(), eps);
  ASSERT_NEAR(a.getY(), b.getY(), eps);
  ASSERT_NEAR(a.getZ(), b.getZ(), eps);
}

void quat_approx(const tf2::Quaternion& a, const tf2::Quaternion& b, double eps)
{
  ASSERT_NEAR(a.getW(), b.getW(), eps);
  ASSERT_NEAR(a.getX(), b.getX(), eps);
  ASSERT_NEAR(a.getY(), b.getY(), eps);
  ASSERT_NEAR(a.getZ(), b.getZ(), eps);
}

TransitionModel create_model()
{
  TransitionModel movement_model(42);
  movement_model.set_translational_noise(POSITION_STANDARD_DEV);
  movement_model.set_rotational_noise(ORIENTATION_STANDARD_DEV);
  movement_model.set_velocity_factor(0);
  return movement_model;
}

tf2::Transform create_0_state()
{
  // start at 0
  return tf2::Transform(tf2::Quaternion(0, 0, 0, 1), tf2::Vector3(0, 0, 0));
}

tf2::Vector3 mult_elementwise(tf2::Vector3 a, const tf2::Vector3& b)
{
  a.setX(a.getX() * b.getX());
  a.setY(a.getY() * b.getY());
  a.setZ(a.getZ() * b.getZ());
  return a;
}

tf2::Quaternion mult_elementwise(tf2::Quaternion a, const tf2::Quaternion& b)
{
  a.setW(a.getW() * b.getW());
  a.setX(a.getX() * b.getX());
  a.setY(a.getY() * b.getY());
  a.setZ(a.getZ() * b.getZ());
  return a;
}

// TEST(TransitionModelTest, TestApplyNoise)
// {
//   auto model = create_model();
//   // Appy 0 mean variance
//   auto state0 = create_0_state();
//   // absolute and square error
//   tf2::Vector3 cum_pos_err(0, 0, 0);
//   tf2::Vector3 cum_pos_err2(0, 0, 0);
//   tf2::Quaternion cum_ori_err(0, 0, 0, 0);
//   tf2::Quaternion cum_ori_err2(0, 0, 0, 0);
//   for (size_t i = 0; i < N_SAMPlES; i++)
//   {
//     auto noise_state = model.apply_noise(state0);
//     tf2::Vector3 pos_err = noise_state.getOrigin() - state0.getOrigin();
//     tf2::Quaternion ori_err = noise_state.getRotation() - state0.getRotation();
//     cum_pos_err += pos_err / N_SAMPlES;
//     cum_pos_err2 += mult_elementwise(pos_err, pos_err) / N_SAMPlES;
//     cum_ori_err += ori_err / N_SAMPlES;
//     cum_ori_err2 += mult_elementwise(ori_err, ori_err) / N_SAMPlES;
//   }
//   vec_approx(tf2::Vector3(0, 0, 0), cum_pos_err, EPS);
//   vec_approx(tf2::Vector3(POSITION_VAR, POSITION_VAR, POSITION_VAR),
//              cum_pos_err2, VAR_EPS);
//   quat_approx(tf2::Quaternion(0, 0, 0, 0), cum_ori_err, EPS);
//   // TODO euler variance in Quaternions???

//   // TODO Apply some random mean
//   state0.setOrigin(tf2::Vector3(123.3, -74.5, 4543.3));
//   state0.setRotation(tf2::Quaternion(0.3726548, 0.7718384, 0.3726548,
//                                      0.3556998));
// }

// TEST(TransitionModelTest, TestPrediction)
// {
//   auto model = create_model();
//   // start at 0
//   auto state0 = create_0_state();

//   // shouldn't change anything
//   auto state1 = model.predict(state0);
//   vec_approx(state0.getOrigin(), state1.getOrigin(), EPS);
//   quat_approx(state0.getRotation(), state1.getRotation(), EPS);
// }

// TEST(MovementModelTest, TestPredictionDynamic)
// {
//   auto model = create_model();
//   // start at 0
//   ObjectState state0 = create_0_state();
//   vec_approx(Vector3(0, 0, 0), state0.position, EPS);
//   quat_approx(Quaternion(1, 0, 0, 0), state0.orientation, EPS);
//   // identity transformation
//   state0.velocity = Vector3(0, 0, 0);
//   state0.twist = Vector3(0, 0, 0);
//   vec_approx(Vector3(0, 0, 0), state0.position, EPS);
//   quat_approx(Quaternion(1, 0, 0, 0), state0.orientation, EPS);
//   // Some simple transitions
//   state0.velocity = Vector3(1, 1, 1);
//   state0.twist = Vector3(M_PI_2, 0, 0);
//   auto state1 = model.predict(state0);
//   vec_approx(Vector3(1, 1, 1), state1.position, EPS);
//   quat_approx(Quaternion(0.707, 0.707, 0, 0), state1.orientation, EPS);
//   // Some simple transitions
//   state1.velocity = Vector3(-0.5, -0.5, -0.5);
//   state1.twist = Vector3(0, M_PI_4, 0);
//   auto state2 = model.predict(state1);
//   vec_approx(Vector3(0.5, 0.5, 0.5), state2.position, EPS);
//   quat_approx(Quaternion(0.653, 0.653, 0.271, -0.271), state2.orientation, EPS);
// }

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
