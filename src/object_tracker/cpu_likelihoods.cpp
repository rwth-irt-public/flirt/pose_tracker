/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <pose_tracker/object_tracker/cpu_likelihoods.hpp>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/shader/shader_builder.hpp>
#include <scigl_render/render/depth_simulator.hpp>
#include <scigl_render/render/rasterizer.hpp>

namespace pose_tracker
{
CpuLikelihoods::CpuLikelihoods(const DepthModelParams& params) : observation_model(params)
{
}

std::vector<double> CpuLikelihoods::log_likelihoods(const DepthImage& observation, const DepthRasterizer& rasterizer)
{
  size_t view_count =
      rasterizer.get_rasterizer().get_views_per_row() * rasterizer.get_rasterizer().get_views_per_column();
  size_t texture_size = rasterizer.get_texture()->get_width() * rasterizer.get_texture()->get_height();
  pixels.reserve(texture_size);
  std::vector<double> log_likelihoods(view_count);
  rasterizer.get_texture()->read_image(pixels.data());
#pragma omp for
  for (size_t i = 0; i < view_count; i++)
  {
    auto img_header =
        rasterizer.get_rasterizer().get_image<float>(i, pixels.data(), scigl_render::DepthSimulator::CHANNELS);
    DepthImage expectation(img_header.rows, img_header.columns, img_header.data, img_header.step);
    log_likelihoods[i] = observation_model.log_likelihood(expectation, observation);
  }
  return log_likelihoods;
}

void CpuLikelihoods::set_params(DepthModelParams params)
{
  observation_model = DepthImageModel(params);
}
}  // namespace pose_tracker
