/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/Eigen/Geometry>
#include <flirt/utility/particle_replacer.hpp>
#include <pose_tracker/conversion/robo_to_scigl.hpp>
#include <pose_tracker/object_tracker/object_tracker.hpp>

namespace pose_tracker
{
ObjectTracker::ObjectTracker(size_t particle_count, TransitionModel transition_model,
                             std::unique_ptr<LikelihoodEvaluator> likelihood_impl, const std::string& mesh_path,
                             const scigl_render::CameraIntrinsics& intrinsics)
  : filter(particle_count)
  , transition_model(std::move(transition_model))
  , depth_rasterizer(intrinsics, mesh_path, particle_count)
  , likelihood_evaluator(std::move(likelihood_impl))
{
  // init states to 1m down the z axis
  ObjectState init_state;
  init_state.position.setZ(1);
  initialize(init_state, 0);
}

void ObjectTracker::initialize(ObjectState state, double delta_t, double replacement_ratio)
{
  std::vector<ObjectState> new_states(filter.get_particle_count(), state);
  // impose randomness on the initialization state
  for (size_t i = 0; i < filter.get_particle_count(); i++)
  {
    new_states[i] = transition_model.predict_noisy(new_states[i], delta_t);
  }
  // resample for correct replacement
  filter.resample();
  auto states = filter.get_states();
  flirt::ParticleReplacer particle_replacer;
  states = particle_replacer.replace(states, new_states, replacement_ratio);
  filter.initialize(std::move(states));
}

void ObjectTracker::predict(const ObjectState& camera_state, double delta_t)
{
  namespace ph = std::placeholders;
  using namespace std::placeholders;
  // predict states and update filter states
  auto predictions = filter.get_states();
  for (auto& state : predictions)
  {
    state = transition_model.predict_noisy(state, delta_t);
  }
  // render expected measurements
  depth_rasterizer.render(camera_state, predictions);
  // update filter
  filter.set_predictions(std::move(predictions));
}

void ObjectTracker::update(const DepthImage& observation)
{
  auto log_likelihoods = likelihood_evaluator->log_likelihoods(observation, depth_rasterizer);
  // update the particle weights and possibly resample
  filter.update(log_likelihoods);
}

ObjectState ObjectTracker::next_frame(const DepthImage& observation, const ObjectState& camera_state, double delta_t)
{
  predict(camera_state, delta_t);
  update(observation);
  return filter.get_map_state();
}

double ObjectTracker::effective_sample_size() const
{
  return filter.effective_sample_size();
}

double ObjectTracker::max_log_likelihood() const
{
  return filter.max_log_likelihood();
}

ObjectState ObjectTracker::mean_state(const std::vector<double>& weights, const std::vector<ObjectState>& states) const
{
  ObjectState mean = {};
  // average quaternions from:
  // https://github.com/fizyr/dr_eigen/blob/master/include/dr_eigen/average.hpp
  Eigen::Matrix<double, 4, 4> A = Eigen::Matrix<double, 4, 4>::Zero();
  for (size_t i = 0; i < weights.size(); i++)
  {
    // quaternion
    Eigen::Matrix<double, 1, 4> q(1, 4);
    q(0) = states[i].orientation.x();
    q(1) = states[i].orientation.y();
    q(2) = states[i].orientation.z();
    q(3) = states[i].orientation.w();
    q *= weights[i];
    A += q.transpose() * q;
    // position
    mean.position += states[i].position * weights[i];
  }
  A /= weights.size();
  Eigen::EigenSolver<Eigen::Matrix<double, 4, 4>> eigen_solver(A);
  Eigen::Matrix<std::complex<double>, 4, 1> mat(eigen_solver.eigenvalues());
  int index;
  mat.real().maxCoeff(&index);
  Eigen::Matrix<double, 4, 1> largest_ev(eigen_solver.eigenvectors().real().block(0, index, 4, 1));
  mean.orientation = tf2::Quaternion(largest_ev(0), largest_ev(1), largest_ev(2), largest_ev(3));
  return mean;
}

}  // namespace pose_tracker