/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <ros/topic.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Float64.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <future>
#include <opencv2/imgproc.hpp>
#include <pose_tracker/gpu/gpu_likelihoods.hpp>
#include <pose_tracker/nodelets/tracker_nodelet.hpp>
#include <pose_tracker/object_tracker/cpu_likelihoods.hpp>
#include <scigl_render_ros/scigl_convert.hpp>
// enable pluginlib for nodelet
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(pose_tracker::TrackerNodelet, nodelet::Nodelet)

namespace ph = std::placeholders;
namespace sr = scigl_render_ros;
namespace enc = sensor_msgs::image_encodings;

namespace pose_tracker
{
// Subscribe to the resized image topic (float in meters)
const std::string CAMERA_IMAGE_TOPIC = "/camera/depth/resized/image_rect";

void TrackerNodelet::camera_callback(const sensor_msgs::ImageConstPtr& depth_image,
                                     const sensor_msgs::CameraInfoConstPtr& info)
{
  if (!object_tracker)
  {
    // lazy initialize here so onInit does not block
    build_tracker(info);
  }
  // only metric depth images supported (see REP118)
  if (depth_image->encoding != sensor_msgs::image_encodings::TYPE_32FC1)
  {
    ROS_WARN("depth image is not a metric 32-bit float image.");
    return;
  }
  auto scigl_image = sr::SciglConvert::convert_ros_image(depth_image, enc::TYPE_32FC1);
  try
  {
    if (use_camera_pose)
    {
      geometry_msgs::TransformStamped camera_tf_msg;
      // wait 0.5 * framerate for transform otherwise get latest
      if (tf_buffer.canTransform(world_frame_id, info->header.frame_id, info->header.stamp,
                                 ros::Duration(0.5 * delta_t)))
      {
        camera_tf_msg = tf_buffer.lookupTransform(world_frame_id, info->header.frame_id, info->header.stamp);
      }
      else
      {
        camera_tf_msg = tf_buffer.lookupTransform(world_frame_id, info->header.frame_id, ros::Time(0));
      }
      tf2::fromMsg(camera_tf_msg.transform.rotation, camera_pose.orientation);
      tf2::fromMsg(camera_tf_msg.transform.translation, camera_pose.position);
    }
    // publish maximum a posteriori transform
    auto map_state = object_tracker->next_frame(scigl_image, camera_pose, delta_t);
    geometry_msgs::TransformStamped object_tf_msg;
    object_tf_msg.header.frame_id = world_frame_id;
    object_tf_msg.header.stamp = info->header.stamp;
    object_tf_msg.child_frame_id = object_frame_id;
    object_tf_msg.transform.rotation = tf2::toMsg(map_state.orientation);
    object_tf_msg.transform.translation = tf2::toMsg(map_state.position);
    // TODO use_camera_pose=false requires to publish in the camera frame
    // it does not hurt to use it for use_camera_pose=true either
    tf_broadcaster.sendTransform(object_tf_msg);
    // effective sample size
    std_msgs::Float64 ess_msg;
    ess_msg.data = object_tracker->effective_sample_size();
    effective_samples_pub.publish(ess_msg);
    // maximum likelihood value
    std_msgs::Float64 max_loglike_msg;
    max_loglike_msg.data = object_tracker->max_log_likelihood();
    max_log_likelihood_pub.publish(max_loglike_msg);
  }
  catch (const tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
  }
}

void TrackerNodelet::build_tracker(const sensor_msgs::CameraInfoConstPtr& info)
{
  // build camera
  auto camera_intrinsics = sr::SciglConvert::convert_camera_info(info, depth_params.min_depth, depth_params.max_depth);
  depth_params.width = camera_intrinsics.width;
  depth_params.height = camera_intrinsics.height;
  camera_frame_id = info->header.frame_id;
  // build tracker
  ROS_INFO("building object tracker");
  gl_context.reset(new scigl_render::GLContext(false, false, depth_params.width, depth_params.height));
  if (use_gpu_likelihoods)
  {
    likelihood_evaluator.reset(new GpuLikelihoods(depth_params));
  }
  else
  {
    likelihood_evaluator.reset(new CpuLikelihoods(depth_params));
  }
  object_tracker.reset(new ObjectTracker(particle_count, transition_model, std::move(likelihood_evaluator), mesh_path,
                                         camera_intrinsics));
  ROS_INFO("object tracker ready");
}

bool TrackerNodelet::init_service_callback(pose_tracker_msgs::InitTracker::Request& request,
                                           pose_tracker_msgs::InitTracker::Response& response)
{
  response.header = request.transform_stamped.header;
  // might not be initialized
  if (!object_tracker)
  {
    ROS_WARN("cannot initialize before receiving a camera image + info");
    return false;
  }
  try
  {
    // reset camera pose
    auto tf_msg = tf_buffer.lookupTransform(world_frame_id, camera_frame_id, ros::Time(0));
    tf2::fromMsg(tf_msg.transform.rotation, camera_pose.orientation);
    tf2::fromMsg(tf_msg.transform.translation, camera_pose.position);
    // reset object pose
    auto parent_to_world = tf_buffer.lookupTransform(world_frame_id, request.transform_stamped.header.frame_id,
                                                     request.transform_stamped.header.stamp, ros::Duration(1));
    geometry_msgs::TransformStamped init_to_world;
    tf2::doTransform(request.transform_stamped, init_to_world, parent_to_world);
    // init the tracker with zero velocities
    ObjectState init_state = {};
    tf2::fromMsg(init_to_world.transform.rotation, init_state.orientation);
    tf2::fromMsg(init_to_world.transform.translation, init_state.position);
    object_tracker->initialize(init_state, delta_t, request.replacement_ratio);
    response.header.stamp = ros::Time::now();
    return true;
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("could not init the object tracker, %s", ex.what());
  }
  return false;
}

void TrackerNodelet::load_parameters()
{
  nh = getNodeHandle();
  auto private_nh = getPrivateNodeHandle();
  // frames
  private_nh.param<std::string>("world_frame_id", world_frame_id, "world");
  private_nh.param<std::string>("object_frame_id", object_frame_id, "object");
  // process model
  double rotational_noise, translational_noise, velocity_factor;
  private_nh.param<double>("translational_noise", translational_noise, 0.001);
  private_nh.param<double>("rotational_noise", rotational_noise, 0.01);
  private_nh.param<double>("velocity_factor", velocity_factor, 0);
  transition_model.set_rotational_noise(rotational_noise);
  transition_model.set_translational_noise(translational_noise);
  transition_model.set_velocity_factor(velocity_factor);
  // observation model
  private_nh.param<std::string>("mesh_path", mesh_path,
                                "/home/tim/sciebo/Robotische "
                                "Schablonen/Versuche_Kunststoff/P3_re_Hockey/track.3ds");
  private_nh.param<double>("depth_factor", depth_params.depth_factor, 1.4e-3);
  private_nh.param<double>("base_noise", depth_params.base_noise, 2e-3);
  private_nh.param<double>("half_life_distance", depth_params.half_life_distance, 1);
  depth_params.normalize_weights();
  private_nh.param<double>("exponential_weight", depth_params.exponential_weight, 0.3);
  private_nh.param<double>("gaussian_weight", depth_params.gaussian_weight, 0.6);
  private_nh.param<double>("uniform_weight", depth_params.uniform_weight, 0.1);
  depth_params.normalize_weights();
  private_nh.param<double>("min_depth", depth_params.min_depth, 0.3);
  private_nh.param<double>("max_depth", depth_params.max_depth, 10);
  // particle filter, no node_handle::param<size_t> so use temporary int
  private_nh.param<int>("particle_count", particle_count, 700);
  private_nh.param<double>("frame_rate", delta_t, 90);
  delta_t = 1.0 / delta_t;
  // use gpu and robot?
  private_nh.param<bool>("use_gpu_likelihoods", use_gpu_likelihoods, true);
  private_nh.param<bool>("use_camera_pose", use_camera_pose, false);
}

void TrackerNodelet::onInit()
{
  auto nh = this->getNodeHandle();
  load_parameters();
  // subscribe tf and camera
  tf_listener.reset(new tf2_ros::TransformListener(tf_buffer, nh));
  image_transport.reset(new image_transport::ImageTransport(nh));
  camera_sub = image_transport->subscribeCamera(CAMERA_IMAGE_TOPIC, 10,
                                                std::bind(&TrackerNodelet::camera_callback, this, ph::_1, ph::_2));
  // publishers
  effective_samples_pub = nh.advertise<std_msgs::Float64>("/effective_samples_size", 1);
  max_log_likelihood_pub = nh.advertise<std_msgs::Float64>("/maximum_log_likelihood", 1);
  // services
  init_service_server = nh.advertiseService("init_tracker", &TrackerNodelet::init_service_callback, this);
  ROS_INFO("started pose initialization service");
}
}  // namespace pose_tracker
