/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <tf2_ros/transform_listener.h>
#include <pose_tracker/model/depth_image_model.hpp>
#include <scigl_render/buffer/frame_buffer.hpp>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/render/depth_simulator.hpp>
#include <scigl_render_ros/scigl_convert.hpp>

// enable pluginlib for nodelet
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(pose_tracker::LikelihoodNormalizer, nodelet::Nodelet)

using scigl_render::DepthSimulator;
using scigl_render_ros::SciglConvert;

namespace pose_tracker
{
TODO;
/**
 * render map state
 * read it into memory
 * calculate the ideal log_likelihood
 * publish the normalized likelihood = max_log_like - ideal_log_like
 */
class LikelihoodNormalizer : public nodelet::Nodelet
{
public:
  /**
   * Initialization with ros_param configuration.
   */
  virtual void onInit();

private:
  ros::NodeHandle nh;
  ros::Subscriber cam_info_sub;

  std::unique_ptr<scigl_render::GLContext> gl_context;
  std::unique_ptr<scigl_render::FrameBuffer> framebuffer;
  std::shared_ptr<scigl_render::Texture2D> texture;
  std::unique_ptr<DepthSimulator> depth_simulator;
  std::string mesh_path;
  std::vector<float> depth_pixels;

  tf2_ros::Buffer tf_buffer;
  std::unique_ptr<tf2_ros::TransformListener> tf_listener;
  std::string world_frame_id, object_frame_id;

  void camera_info_callback(const sensor_msgs::CameraInfoConstPtr& camera_info);
  /**
   * Inits the offscreen depth rendering context
   */
  void init_opengl(scigl_render::CvCamera camera);

  /**
   * Render the current estimate
   */
  void render(const scigl_render::QuaternionPose& camera_pose, const scigl_render::QuaternionPose& object_pose);

  /**
   * Calculates the normalized log_likelihood for the rendered depth image
   * of the texture
   */
  double normalized_log_likelihood();
};

void LikelihoodNormalizer::onInit()
{
  nh = getNodeHandle();
  auto pnh = getPrivateNodeHandle();
  // params
  pnh.param<std::string>("mesh_path", mesh_path,
                         "/home/tim/sciebo/Robotische "
                         "Schablonen/Versuche_Kunststoff/P3_re_Hockey/track.3ds");
  pnh.param<std::string>("world_frame_id", world_frame_id, "world");
  pnh.param<std::string>("object_frame_id", object_frame_id, "object");
  // subscribe
  tf_listener.reset(new tf2_ros::TransformListener(tf_buffer));
  cam_info_sub = nh.subscribe<sensor_msgs::CameraInfo>(
      "camera/depth/camera_info", 5,
      std::bind(&LikelihoodNormalizer::camera_info_callback, this, std::placeholders::_1));
}

void LikelihoodNormalizer::camera_info_callback(const sensor_msgs::CameraInfoConstPtr& camera_info)
{
  if (!depth_simulator)
  {
    // lazy initialize the depth simulator so onInit does not block
    init_opengl(SciglConvert::convert_camera_info(camera_info));
  }
  try
  {
    // transforms for the camera and object
    auto current_time = camera_info->header.stamp;
    auto camera_tf =
        tf_buffer.lookupTransform(world_frame_id, camera_info->header.frame_id, current_time, ros::Duration(1. / 30));
    auto object_tf = tf_buffer.lookupTransform(world_frame_id, object_frame_id, current_time, ros::Duration(1. / 30));
    auto camera_pose = SciglConvert::convert_tf(camera_tf);
    auto object_pose = SciglConvert::convert_tf(object_tf);
    // render and evaluate the expectation
    render(camera_pose, object_pose);
    texture->read_image(depth_pixels.data());
    DepthImage image(texture->get_height(), texture->get_width(), depth_pixels.data(), 0);
  }
  catch (const tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
  }
  geometry_msgs::TransformStamped tf_msg;
  if (tf_buffer.canTransform(world_frame_id, info->header.frame_id, info->header.stamp))
  {
    // get specific transform if available
    tf_msg = tf_buffer.lookupTransform(world_frame_id, info->header.frame_id, info->header.stamp);
  }
  else
  {
    // get latest otherwise
    tf_msg = tf_buffer.lookupTransform(world_frame_id, info->header.frame_id, ros::Time(0));
  }
}

void LikelihoodNormalizer::init_opengl(scigl_render::CvCamera camera)
{
  // offscreen render context
  gl_context.reset(
      new scigl_render::GLContext(false, false, camera.get_intrinsics().width, camera.get_intrinsics().height));
  texture = std::make_shared<scigl_render::Texture2D>(camera.get_intrinsics().width, camera.get_intrinsics().height,
                                                      DepthSimulator::FORMAT, DepthSimulator::INTERNAL_FORMAT,
                                                      DepthSimulator::TYPE);
  framebuffer.reset(new scigl_render::FrameBuffer(texture));
  depth_pixels.reserve(texture->get_width() * texture->get_height());
  // scene simulation
  depth_simulator.reset(new DepthSimulator(std::move(camera), scigl_render::Model(mesh_path)));
}

void LikelihoodNormalizer::render(const scigl_render::QuaternionPose& camera_pose,
                                  const scigl_render::QuaternionPose& object_pose)
{
  framebuffer->clear();
  framebuffer->activate();
  depth_simulator->render_pose(object_pose, camera_pose);
}

double normalized_log_likelihood()
{
  // TODO need observation and the observation model
}

// double
// calc_ideal_log_likelihood(const DepthImage& expectation);
// {
//   // score of the maximum_likelihood observation
//   auto max_likelihood_element =
//       std::max_element(log_likelihoods.begin(), log_likelihoods.end());
//   auto max_likelihood_index =
//       std::distance(log_likelihoods.begin(), max_likelihood_element);
//   std::vector<float>
//   pixels(depth_rasterizer.get_rasterizer().get_view_size()); auto view_xy =
//   depth_rasterizer.get_rasterizer().get_view_xy();
//   depth_rasterizer.get_framebuffer()->read_image(
//       pixels.data(), view_xy.first, view_xy.second,
//       depth_rasterizer.get_rasterizer().get_view_width,
//       depth_rasterizer.get_rasterizer().get_view_height);
//   DepthImage expectation(depth_rasterizer.get_rasterizer().get_view_width(),
//                          depth_rasterizer.get_rasterizer().get_view_height(),
//                          pixels.data(), 0);
// }
}  // namespace pose_tracker