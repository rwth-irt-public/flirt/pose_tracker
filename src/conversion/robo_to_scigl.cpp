/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <pose_tracker/conversion/robo_to_scigl.hpp>

namespace pose_tracker
{
auto RoboToScigl::convert_pose(const ObjectState& state) -> scigl_render::QuaternionPose
{
  scigl_render::QuaternionPose scigl_pose;
  scigl_pose.position.x = state.position.getX();
  scigl_pose.position.y = state.position.getY();
  scigl_pose.position.z = state.position.getZ();
  scigl_pose.orientation.w = state.orientation.getW();
  scigl_pose.orientation.x = state.orientation.getX();
  scigl_pose.orientation.y = state.orientation.getY();
  scigl_pose.orientation.z = state.orientation.getZ();
  return scigl_pose;
}
}  // namespace pose_tracker