/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <flirt/utility/log_arithmetics.hpp>
#include <pose_tracker/constants.hpp>
#include <pose_tracker/model/depth_pixel_model.hpp>

namespace pose_tracker
{
DepthPixelModel::DepthPixelModel(DepthModelParams measurement_params) : params(std::move(measurement_params))
{
  // Make sure that the sum of the weights is 1
  params.normalize_weights();
}

double DepthPixelModel::log_likelihood(float expected, float observed) const
{
  if(!is_valid(expected)){
    // expected invalid: Any measurement is okay log(1)=0, check this condition first!
    return 0;
  }
  if (!is_valid(observed))
  {
    // observed invalid: exponential would divide by 0, normal is so small that log would return -inf
    return std::log(params.uniform_weight * params.get_uniform_density());
  }
  if (observed <= expected)
  {
    // occlusion is possible
    return std::log(params.uniform_weight * params.get_uniform_density() +
                    params.gaussian_weight * gaussian_probability(expected, observed) +
                    params.exponential_weight * exponential_probability(params.get_lambda(), expected, observed));
  }
  else
  {
    // occlusion impossible
    return std::log(params.uniform_weight * params.get_uniform_density() +
                    params.gaussian_weight * gaussian_probability(expected, observed));
  }
}

double DepthPixelModel::ideal_log_likelihood(const float& expected) const
{
  // integrate over density function and divide by intervall (one sigma)
  double sigma = sigma_for_depth(expected);
  return THREE_SIGMA_PERCENT / (2 * sigma);
}

bool DepthPixelModel::is_valid(float depth) const
{
  return depth > 0 && depth < params.max_depth;
}

double DepthPixelModel::sigma_for_depth(float expected_depth) const
{
  return params.depth_factor * square(expected_depth) + params.base_noise;
}

double DepthPixelModel::gaussian_probability(float expected, float observed) const
{
  double sigma = sigma_for_depth(expected);
  double two_variance = 2 * square(sigma);
  // normalization factor is approx 1
  double res = std::exp(-square(observed - expected) / two_variance) / std::sqrt(M_PI * two_variance);
  return res;
}

double DepthPixelModel::exponential_probability(float lambda, float expected, float observed) const
{
  double numerator = std::exp(-lambda * observed);
  double denominator = 1 - std::exp(-lambda * expected);
  return lambda * numerator / denominator;
}
}  // namespace pose_tracker