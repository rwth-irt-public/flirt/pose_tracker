/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <pose_tracker/constants.hpp>
#include <pose_tracker/model/depth_image_model.hpp>

namespace pose_tracker {
DepthImageModel::DepthImageModel(DepthModelParams depth_params)
    : pixel_model(DepthPixelModel(std::move(depth_params))) {}

double DepthImageModel::log_likelihood(const DepthImage &expectation,
                                       const DepthImage &observation) const {
  // invalid observations are impossible
  if (expectation.rows != observation.rows ||
      expectation.cols != observation.cols) {
    throw std::runtime_error("image sizes do not match");
  }
  if (!observation.isContinuous()) {
    throw std::runtime_error("observation image is not continous");
  }
  if (observation.channels() != 1) {
    throw std::runtime_error("observation does not have exactly one channel");
  }
  // independent measurements -> joint distribution is product
  // log_likelihood -> product changes to sum
  double log_likelihood = 0;
  for (int row = 0; row < expectation.rows; row++) {
    auto expectation_row = expectation.ptr<float>(row);
    auto observation_row = observation.ptr<float>(row);
    for (int col = 0; col < expectation.cols; col++) {
      log_likelihood += pixel_model.log_likelihood(expectation_row[col],
                                                   observation_row[col]);
    }
  }
  return log_likelihood;
}

double DepthImageModel::ideal_log_likelihood(const DepthImage &expectation) {
  double ideal_log_likelihood = 0;
  for (int row = 0; row < expectation.rows; row++) {
    auto expectation_row = expectation.ptr<float>(row);
    for (int col = 0; col < expectation.cols; col++) {
      ideal_log_likelihood +=
          pixel_model.ideal_log_likelihood(expectation_row[col]);
    }
  }
  return ideal_log_likelihood;
}
}  // namespace pose_tracker