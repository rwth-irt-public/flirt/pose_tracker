/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <flirt/utility/normal_sampler.hpp>
#include <pose_tracker/model/transition_model.hpp>

namespace pose_tracker
{
TransitionModel::TransitionModel()
{
  normal_sampler = std::make_shared<flirt::NormalSampler>();
}

TransitionModel::TransitionModel(unsigned int seed)
{
  normal_sampler = std::make_shared<flirt::NormalSampler>(seed);
}

tf2::Vector3 TransitionModel::random_acceleration()
{
  return tf2::Vector3(normal_sampler->draw_normal(0.0, translational_noise),
                      normal_sampler->draw_normal(0.0, translational_noise),
                      normal_sampler->draw_normal(0.0, translational_noise));
}

tf2::Vector3 TransitionModel::random_angular_acceleration()
{
  return tf2::Vector3(normal_sampler->draw_normal(0.0, rotational_noise),
                      normal_sampler->draw_normal(0.0, rotational_noise),
                      normal_sampler->draw_normal(0.0, rotational_noise));
}

ObjectState TransitionModel::predict(ObjectState state, tf2::Vector3 acceleration, tf2::Vector3 angular_acceleration,
                                     double delta_t) const
{
  // Calculate the change of the states via Euler forward / Taylor approximations
  // x_dot = dt*x_dot_dot
  // x = dt*x_dot + 0.5*dt^2*x_dot_dot
  auto angular_velocity_change = delta_t * angular_acceleration;
  auto velocity_change = delta_t * acceleration;
  auto orientation_change = delta_t * state.angular_velocity + 0.5 * delta_t * angular_velocity_change;
  // create quaternion from the angle rate, mind division by zero if amount of change is zero
  tf2::Quaternion quaternion_change(0, 0, 0, 1);
  if (orientation_change.length2() != 0)
  {
    // amount of the vector corresponds to the angle
    quaternion_change.setRotation(orientation_change.normalized(), orientation_change.length());
  }
  auto position_change = delta_t * state.velocity + 0.5 * delta_t * velocity_change;
  // Update the state, the derivatives must be updated last for an inplace update
  state.orientation *= quaternion_change;
  state.orientation.normalize();
  state.position += position_change;
  // Decay the velocities from the last time-step
  state.angular_velocity = velocity_factor * state.angular_velocity + angular_velocity_change;
  state.velocity = velocity_factor * state.velocity + velocity_change;
  return state;
}

ObjectState TransitionModel::predict_noisy(ObjectState state, double delta_t)
{
  return predict(state, random_acceleration(), random_angular_acceleration(), delta_t);
}

void TransitionModel::set_translational_noise(double standard_deviation)
{
  this->translational_noise = standard_deviation;
}

void TransitionModel::set_rotational_noise(double standard_deviation)
{
  this->rotational_noise = standard_deviation;
}

void TransitionModel::set_velocity_factor(double factor)
{
  velocity_factor = factor;
}

}  // namespace pose_tracker
