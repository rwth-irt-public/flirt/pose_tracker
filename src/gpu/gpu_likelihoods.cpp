/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <pose_tracker/gpu/gpu_likelihoods.hpp>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/shader/shader_builder.hpp>
#include <scigl_render/render/depth_simulator.hpp>

const GLuint OBSV_BINDING = 0;
const GLuint EXPC_BINDING = 1;
const GLuint LIKE_BINDING = 2;

namespace pose_tracker
{
GpuLikelihoods::GpuLikelihoods()
{
  // build shader
  scigl_render::ShaderBuilder builder;
  const std::string comp_source =
#include <pose_tracker/gpu/likelihoods.comp>
      "";
  builder.attach_compute_shader(comp_source);
  compute_shader = builder.build();
  // generate buffer storage
  glGenBuffers(1, &likelihoods_ssbo);
  scigl_render::check_gl_error("created gpu likelihoods");
}

GpuLikelihoods::GpuLikelihoods(const DepthModelParams &params)
    : GpuLikelihoods()
{
  set_params(params);
}

std::vector<double> GpuLikelihoods::log_likelihoods(
    const DepthImage &observation,
    const DepthRasterizer &rasterizer)
{
  set_observation(observation);
  set_excpectations(rasterizer.get_texture(),
                    rasterizer.get_rasterizer().get_views_per_row(),
                    rasterizer.get_rasterizer().get_views_per_column());
  // wait for rendering and image tansfer
  glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
  // execute the compute shader
  compute_shader.activate();
  glDispatchCompute(views_per_row, views_per_column, 1);
  compute_shader.deactivate();
  // wait for the results and read them back to the CPU
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, likelihoods_ssbo);
  glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);
  std::vector<float> buffer(views_per_row * views_per_column);
  glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0,
                     buffer.size() * sizeof(float), buffer.data());
  // unbind and return
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
  return std::vector<double>(buffer.begin(), buffer.end());
}

void GpuLikelihoods::set_params(DepthModelParams params)
{
  compute_shader.setFloat("base_noise", params.base_noise);
  compute_shader.setFloat("depth_factor", params.depth_factor);
  params.normalize_weights();
  compute_shader.setFloat("exponential_weight", params.exponential_weight);
  compute_shader.setFloat("gaussian_weight", params.gaussian_weight);
  compute_shader.setUVec2("image_size", params.width, params.height);
  compute_shader.setFloat("lambda", params.get_lambda());
  compute_shader.setFloat("max_depth", params.max_depth);
  compute_shader.setFloat("min_depth", params.min_depth);
  compute_shader.setFloat("uniform_weight", params.uniform_weight);
  compute_shader.setFloat("uniform_density", params.get_uniform_density());
  glMemoryBarrier(GL_UNIFORM_BARRIER_BIT);
  scigl_render::check_gl_error("set the depth model params");
  // allocate the observation texture
  observation_tex.reset(new scigl_render::Texture2D(
      params.width, params.height,
      scigl_render::DepthSimulator::FORMAT,
      scigl_render::DepthSimulator::INTERNAL_FORMAT,
      scigl_render::DepthSimulator::TYPE));
  scigl_render::check_gl_error("created observation texture");
}

void GpuLikelihoods::set_excpectations(
    std::shared_ptr<scigl_render::Texture2D> expectations,
    size_t vpr, size_t vpc)
{
  expectations->bind_image_unit(EXPC_BINDING, GL_READ_ONLY);
  scigl_render::check_gl_error("bound expectations image unit");
  // resize the likelihood array in shader?
  if (vpr != views_per_row || vpc != views_per_column)
  {
    views_per_row = vpr;
    views_per_column = vpc;
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, likelihoods_ssbo);
    glBufferData(GL_SHADER_STORAGE_BUFFER, vpr * vpc * sizeof(float), NULL,
                 GL_STATIC_READ);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, LIKE_BINDING, likelihoods_ssbo);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
  }
}

void GpuLikelihoods::set_observation(const DepthImage &observation) const
{
  observation_tex->store_image(observation.data);
  observation_tex->bind_image_unit(OBSV_BINDING, GL_READ_ONLY);
}
} // namespace pose_tracker