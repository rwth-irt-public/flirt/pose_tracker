/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <pose_tracker/gpu/depth_rasterizer.hpp>
#include <pose_tracker/conversion/robo_to_scigl.hpp>
#include <scigl_render/check_gl_error.hpp>

namespace pose_tracker
{
DepthRasterizer::DepthRasterizer(
    const scigl_render::CameraIntrinsics &intrinsics,
    const std::string &mesh_path,
    size_t particle_count)
    : depth_simulator(scigl_render::CvCamera(intrinsics),
                      scigl_render::Model(mesh_path)),
      rasterizer(intrinsics.width, intrinsics.height, 0, 0)
{
  // minimal raster dimensions
  auto raster_size = scigl_render::Rasterizer::calc_min_raster(
      particle_count, depth_simulator.get_width(),
      depth_simulator.get_height(), scigl_render::FrameBuffer::get_max_size());
  rasterizer.set_views_per_row(raster_size.first);
  rasterizer.set_views_per_column(raster_size.second);
  // framebuffer for the raster
  texture = std::make_shared<scigl_render::Texture2D>(
      rasterizer.get_texture_width(), rasterizer.get_texture_height(),
      scigl_render::DepthSimulator::FORMAT,
      scigl_render::DepthSimulator::INTERNAL_FORMAT,
      scigl_render::DepthSimulator::TYPE);
  scigl_render::check_gl_error("created raster texture");
  framebuffer = std::make_shared<scigl_render::FrameBuffer>(texture);
  scigl_render::check_gl_error("created depth framebuffer");
}

void DepthRasterizer::render(const ObjectState &camera_state,
                             const std::vector<ObjectState> &object_poses)
{
  rasterizer.activate_all();
  framebuffer->clear();
  framebuffer->activate();
  auto camera_pose = RoboToScigl::convert_pose(camera_state);
  for (size_t i = 0; i < object_poses.size(); i++)
  {
    rasterizer.activate_view(i);
    depth_simulator.render_pose(
        RoboToScigl::convert_pose(object_poses[i]),
        camera_pose);
  }
  framebuffer->deactivate();
}

auto DepthRasterizer::get_texture() const
    -> std::shared_ptr<scigl_render::Texture2D>
{
  return texture;
}
auto DepthRasterizer::get_framebuffer() const
    -> std::shared_ptr<scigl_render::FrameBuffer>
{
  return framebuffer;
}
scigl_render::Rasterizer DepthRasterizer::get_rasterizer() const
{
  return rasterizer;
}

} // namespace pose_tracker